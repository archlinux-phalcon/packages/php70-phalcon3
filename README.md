# Phalcon Framework Arch Linux repository

Unofficial auto-updated repository for personal use. Feel free to use/reuse/publish.

```
[archlinux-phalcon]
SigLevel = Never
Server = https://archlinux-phalcon.gitlab.io/repository
```

All credits goes to [archlinux-aur](https://gitlab.com/archlinux-aur/support/wikis/how-it-works).
